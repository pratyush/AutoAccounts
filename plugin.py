###
# Copyright (c) 2021, mogad0n
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#   * Redistributions of source code must retain the above copyright notice,
#     this list of conditions, and the following disclaimer.
#   * Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions, and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#   * Neither the name of the author of this software nor the name of
#     contributors to this software may be used to endorse or promote products
#     derived from this software without specific prior written consent.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

###

from supybot import utils, plugins, ircutils, callbacks, log, conf, irclib, ircmsgs, ircdb
from supybot.commands import *
try:
    from supybot.i18n import PluginInternationalization
    _ = PluginInternationalization('AutoAccounts')
except ImportError:
    # Placeholder that allows to run the plugin on a bot
    # without the i18n module
    _ = lambda x: x

import re
import os
import sys
import time

class AutoAccounts(callbacks.Plugin):
    """Test generating Limnoria accounts automatically for a single network oragonoIRCd instance"""
    threaded = True

    # Assumes being set up with OPER and also the appropriate snomasks.
    # Read for Account Registrations
    # Since oragono has unique and permanent account names the major issue with this is solved for now.
    # multi network bots simple won't be able to satisfy all the other concerns brought up in #1414
    # SNOTICES are pretty much garbage now with every ircd doing their own thing.. If and when machine-readable
    # SNOTICES can be agreed upon much of this vulnerable, inflexible code will be replaced.
    def doNotice(self, irc, msg):
        (target, text) = msg.args
        if target == irc.nick:
            text = ircutils.stripFormatting(text)
            if 'ACCOUNT' in text and 'registered account' in text:
                accregregex = "^-ACCOUNT- Client \[(.*)\] registered account \[(.*)\] from IP (.*)"
                couple = re.match(accregregex, text)
                nuh = couple.group(1)
                account = couple.group(2)
                ip = couple.group(3)
                vhost = f"libcasa.user.{account}" # add the network as a config variable.
                DictFromReg = {'notice': 'accreg', 'account': account, 'og_nug': nuh, 'ip': ip, 'vhosts': vhost}
                # add to whatever db and create a table for this account to track their clients, infractions etc.
                self._setVhost(irc, msg, DictFromReg)
        
    def _setVhost(self, irc, msg, DictFromReg):
        arg = ['SET', DictFromReg['account'], DictFromReg['vhost']]
        irc.queueMsg(msg=ircmsgs.IrcMsg(command='HS',
                    args=arg))
    
    def _generateUserAccnt(self, irc, msg, DictFromReg, X):
        # https://github.com/ProgVal/Limnoria/blob/6f6dad8f7ba71ac9c6285a143097f30748114c7c/plugins/User/plugin.py#L114
        addHostmask = True
        name = DictFromReg['account']

        try:
            ircdb.users.getUserId(name)
            irc.error(_('That name is already assigned to someone.'),
                      Raise=True)
        except KeyError:
            pass
        try:
            u = ircdb.users.getUser(msg.prefix)
            if u._checkCapability('owner'):
                addHostmask = False
            else:
                irc.error(_('Your hostmask is already registered to %s') %
                          u.name)
                return
        except KeyError:
            pass
        user = ircdb.users.newUser()
        user.name = name
        X = 1 # PLACE HOLDER ..replace passwords with something.
        user.setPassword(X)
        if addHostmask:
            user.addHostmask(msg.prefix)
        ircdb.users.setUser(user)
        # irc.replySuccess()
        # ^ replace with appropriate privmsg to the user
        # you can add bot help menu, app listings, server stats,
        # as a response to successfull detection and creation of your account.
        # obviously optional.




Class = AutoAccounts


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
